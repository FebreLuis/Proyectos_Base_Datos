package Controlador;

import Modelo.DAO;
import Modelo.DATTienda;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Usuario
 */
@WebServlet(urlPatterns = {"/ConsultaFacturaComprasProveedor.do"})
public class ConsultaFacturaComprasProveedor extends HttpServlet {

    DATTienda objDat = new DATTienda();
    DAO objDao = new DAO();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        double calcula = 0;

        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Factura Compras</title>");
        out.println("</head>");
        out.println("</body>");

        out.println(" <form action= ConsultaFacturaComprasProveedor.do method=post>");

        out.println("<table >");

        out.println(" <tr><td>PROVEEDOR</td><td><input type=text name=PROVEEDOR ></td></tr><br>");

        out.println(" </table>");

        out.println("<br>");

        out.println("<input class = boton type=submit name=Registrar value=Buscar  style= 'text-align:center ;'>");
        out.println("</form>");

        out.println("</body>");
        out.println("</html>");

        String id = request.getParameter("PROVEEDOR");

        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Factura Compras</title>");
        out.println("<link rel=stylesheet type=text/css href=tablain.css>");
        out.println("</head>");
        out.println("<body>");

        out.println("<center>");

        out.println("<h1>Factura Ventas </h1>");
        out.println("<a style='text-decoration:none; position: relative;' href='index.jsp'><b>INICIO</b></a>");

        out.println("<table border='1'>");
        out.println("<tr>");
        out.println("<td>NOMBRE PROVEEDOR</td>");
        out.println("<td>DIRECCION PROVEEDOR</td>");
        out.println("<td>NUMERO FACTURA</td>");
        out.println("<td>FECHA</td>");
        out.println("<td>NUMERO DETALLE</td>");
        out.println("<td>NOMBRE PRODUCTO</td>");
        out.println("<td>CANTIDAD</td>");
        out.println("<td>PRECIO</td>");
        out.println("<td>IVA</td>");
        out.println("<td>TOTAL</td>");
        out.println("</tr>");

        ResultSet rs = objDat.consultar_FacturaCompra_Provee(id);
        while (rs.next()) {
            // out.println("<a style='text-decoration:none; position: relative;' href='ConsultarFactura_Cedula.view'><b>MOSTRAR TODO</b></a>");

            out.println("<tr>");
            out.println("<td>" + rs.getString("NOMBRE_PROVE") + "</td>");
            out.println("<td>" + rs.getString("DIRECCION_PROVE") + "</td>");
            out.println("<td>" + rs.getString("NUM_FACTURA_C") + "</td>");
            out.println("<td>" + rs.getString("FECHA_CO") + "</td>");
            out.println("<td>" + rs.getString("NUMERO_DETALLE_C") + "</td>");
            out.println("<td>" + rs.getString("NOMBRE_PRO") + "</td>");
            out.println("<td>" + rs.getString("CANTIDAD_C") + "</td>");
            out.println("<td>" + rs.getString("PRECIO_C") + "</td>");
            out.println("<td>" + rs.getString("IVA_C") + "</td>");
            out.println("<td>" + rs.getString("TOTAL") + "</td>");
            out.println("</tr>");

            String total = rs.getString("TOTAL");
            double trasTot = Double.parseDouble(total);
            calcula = trasTot + calcula;

        }

     
        out.println("</table>");
        out.println("<br>");
        out.println("<h3style='padding-right: 404px;'; >Total</h3>");
        out.println("<input  value = " + calcula + ">");
        out.println("</center>");
        out.println("</body>");
        out.println("</html>");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConsultaFacturaComprasProveedor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ConsultaFacturaComprasProveedor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConsultaFacturaComprasProveedor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ConsultaFacturaComprasProveedor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
