package Controlador;

import CLASES.Empleado;
import Modelo.DAO;
import Modelo.DATTienda;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Usuario
 */
@WebServlet(urlPatterns = {"/Modificar_Empleado.do"})
public class Modificar_Empleado extends HttpServlet {

    DATTienda objDat = new DATTienda();
    DAO objDao = new DAO();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        DAO objDao = new DAO();
        ArrayList<Empleado> lista = objDao.Empleado();

        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Modificar Empleados</title>");
        out.println("<meta charset=utf-8>");
        out.println("<link rel=stylesheet type=text/css href=tablain.css>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1>Empleados Registrados </h1>");
        out.println("<center>");
        out.println("<a style='text-decoration:none; position: relative;' href='index.jsp'><b>INICIO</b></a>");
        out.println("<form action='ConsultarEmpleados.view' method='post'>");

        out.println("</form>");

        out.println("<table border='1'>");
        out.println("<tr>");
        out.println("<td>ID_EMPLE</td>");
        out.println("<td>COD_EMPLE</td>");
        out.println("<td>NOMBRE</td>");
        out.println("<td>SUELDO</td>");
        out.println("<td>CARGO</td>");

        out.println("</tr>");

        for (Empleado w : lista) {
            out.println("<tr>");
            out.println("<td>" + w.getId_emple() + "</td>");
            out.println("<td>" + w.getCod_emple() + "</td>");
            out.println("<td>" + w.getNombre() + "</td>");
            out.println("<td>" + w.getSueldo() + "</td>");
            out.println("<td>" + w.getCargo() + "</td>");

            out.println("</tr>");
        }
        out.println("</table>");

        out.println(" <form action= Modificar_Empleado.do method=post>");

        out.println("<table >");

        out.println(" <tr><td>Id Empleado</td><td><input type=text name=Id ></td></tr><br>");
        out.println(" <tr><td>Codigo Empleado</td><td><input type=text name=Codigo_Empleado ></td></tr><br>");
        out.println(" <tr><td>Nombres</td><td><input type=text name=nombres ></td></tr><br>");
        out.println(" <tr><td>Sueldo</td><td><input type=text name=sueldo ></td></tr><br>");
        out.println(" <tr><td>Cargo</td><td><input type=text name=cargo ></td></tr><br>");

        out.println(" </table>");

        out.println("<br>");

        out.println("<input class = boton type=submit name=Cambiar value=Modificar  style= 'text-align:center ;'>");
        out.println("</form>");

        out.println("</center>");
        out.println("</body>");
        out.println("</html>");
        out.println("</body>");
        out.println("</html>");

        String id = request.getParameter("Id");
        String codigo = request.getParameter("Codigo_Empleado");
        String nombres = request.getParameter("nombres");
        String sueldo = request.getParameter("sueldo");
        String cargo = request.getParameter("cargo");

        out.println(" <form action= Modificar_Empleado.do method=post>");

        out.println("<table >");

        out.println(" </table>");

        out.println("<br>");

        out.println("<input class = boton type=submit name=Cambiar value=Actualizar  style= 'text-align:center ;'>");
        out.println("</form>");

        int tras = Integer.parseInt(id);
        double tras2 = Double.parseDouble(sueldo);
        objDat.ModificarEmpleado(codigo, nombres, tras2, cargo, tras);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Modificar_Empleado.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Modificar_Empleado.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Modificar_Empleado.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Modificar_Empleado.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
