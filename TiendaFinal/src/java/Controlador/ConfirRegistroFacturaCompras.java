package Controlador;

import Modelo.DATConexion;
import Modelo.DATTienda;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author febre
 */
@WebServlet(urlPatterns = {"/ConfirRegistroFacturaCompras.do"})
public class ConfirRegistroFacturaCompras extends HttpServlet {

    DATConexion c = new DATConexion();
    DATTienda objDat = new DATTienda();

    public ConfirRegistroFacturaCompras() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        String id_prove = request.getParameter("ID_PROVEEDOR");
        String fecha = request.getParameter("FECHA_CO");

        int tras1 = Integer.parseInt(id_prove);

        int resultado = objDat.ingresarFacturaCompras(tras1, fecha);

        if (resultado > 0) {
            out.println(" <!DOCTYPE html>");
            out.println("<html>");
            out.println("<body style=' background: #AED6F1; text-align: center' ;>");
            out.println("<h1> FACTURA RESGISTRADA </h1>");
            out.println("<a style='text-decoration:none; position: absolute; left:500px;' href='RegistrarFacturaCompras.view'><b>VOLVER A REGITRAR</b></a>");
            out.println("<a style='text-decoration:none; position: relative; left:80px;' href='index.jsp'><b>INICIO</b></a>");
            out.println("</body>");
            out.println("</html>");
        } else {
            out.println(" <!DOCTYPE html>");
            out.println("<html>");
            out.println("<body style=' background: #AED6F1 ; text-align: center;'>");
            out.println("<h1> ERROR</h1>");
            out.println("<a style='text-decoration:none; position: absolute; left:500px;' href='RegistrarFacturaCompras.view'><b>VOLVER A INTENTAR</b></a>");
            out.println("<a style='text-decoration:none; position: relative; left:80px;' href='index.jsp'><b>INICIO</b></a>");
            out.println("</body>");
            out.println("</html>");
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
