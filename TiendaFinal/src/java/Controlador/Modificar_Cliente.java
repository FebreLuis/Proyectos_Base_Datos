package Controlador;

import CLASES.Clientes;
import CLASES.Empleado;
import Modelo.DAO;
import Modelo.DATTienda;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Usuario
 */
@WebServlet(urlPatterns = {"/Modificar_Cliente.do"})
public class Modificar_Cliente extends HttpServlet {

    DATTienda objDat = new DATTienda();
    DAO objDao = new DAO();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        DAO objDao = new DAO();
       ArrayList<Clientes> lista = objDao.Clientes();

        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Modificar Clientes</title>");
        out.println("<link rel=stylesheet type=text/css href=tablain.css>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1>Clientes Registrados</h1>");
        out.println("<center>");
        out.println("<a style='text-decoration:none; position: relative;' href='index.jsp'><b>INICIO</b></a>");
        out.println("<form action='ConsultarClientes.view' method='post'>");

        out.println("</form>");

        out.println("<table border='1'>");
        out.println("<tr>");
        out.println("<td>ID CLIENTE</td>");
        out.println("<td>CEDULA</td>");
        out.println("<td>NOMBRES</td>");
        out.println("<td>TELEFONO</td>");
        out.println("<td>DIRECCION</td>");

        out.println("</tr>");

        for (Clientes w : lista) {
            out.println("<tr>");
            out.println("<td>" + w.getId_cliente() + "</td>");
            out.println("<td>" + w.getCedula() + "</td>");
            out.println("<td>" + w.getNombres() + "</td>");
            out.println("<td>" + w.getTelefono() + "</td>");
            out.println("<td>" + w.getDireccion() + "</td>");
            out.println("</tr>");
        }
        out.println("</table>");
       

        // out.println("<a href='index.html'>Volver</a>");
        

        out.println(" <form action=Modificar_Cliente.do method=post>");

        out.println("<table >");

        out.println(" <tr><td>Id Cliente</td><td><input type=text name=Id ></td></tr><br>");
        out.println(" <tr><td>Nombres</td><td><input type=text name=Nombres ></td></tr><br>");
        out.println(" <tr><td>Telefono</td><td><input type=text name=telefono ></td></tr><br>");
        out.println(" <tr><td>Direccion</td><td><input type=text name=direccion ></td></tr><br>");

        out.println(" </table>");

        out.println("<br>");

        out.println("<input class = boton type=submit name=Cambiar value=Modificar  style= 'text-align:center ;'>");
        out.println("</form>");

        out.println("</center>");
        out.println("</body>");
        out.println("</html>");
        out.println("</body>");
        out.println("</html>");

        String id = request.getParameter("Id");
        String nombres = request.getParameter("Nombres");
        String telefono = request.getParameter("telefono");
        String direccion = request.getParameter("direccion");

        out.println(" <form action=Modificar_Cliente.do method=post>");

        out.println("<table >");

        out.println(" </table>");

        out.println("<br>");

        out.println("<input class = boton type=submit name=Cambiar value=Actualizar  style= 'text-align:center ;'>");
        out.println("</form>");

        int tras = Integer.parseInt(id);
        objDat.modificarClientes(nombres, telefono, direccion, tras);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Modificar_Cliente.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Modificar_Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Modificar_Cliente.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Modificar_Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
