package Controlador;

import Modelo.DAO;
import Modelo.DATTienda;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Usuario
 */
@WebServlet(urlPatterns = {"/EliminarFacturaCompras.do"})
public class EliminarFacturaCompras extends HttpServlet {

    DATTienda objDat = new DATTienda();
    DAO objDao = new DAO();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
       

        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Eliminar Factura Compras</title>");
        out.println("</head>");
        out.println("</body>");
        out.println(" <form action= EliminarFacturaCompras.do method=post>");
        out.println("<table >");
        out.println(" <tr><td>PROVEEDOR</td><td><input type=text name= proveedor ></td></tr><br>");
        out.println(" </table>");
        out.println("<br>");

        out.println("<input class = boton type=submit name=Registrar value=Buscar  style= 'text-align:center ;'>");
        out.println("</form>");

        out.println("</body>");
        out.println("</html>");

        String id = request.getParameter("proveedor");

        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Factura Compras</title>");
        out.println("<link rel=stylesheet type=text/css href=tablain.css>");
        out.println("</head>");
        out.println("<body>");

        out.println("<center>");

        out.println("<h1>Detalle Compras </h1>");
        out.println("<a style='text-decoration:none; position: relative;' href='index.jsp'><b>INICIO</b></a>");

        out.println("<table border='1'>");
        out.println("<tr>");
        out.println("<td>NUMERO DETALLE</td>");
        out.println("<td>NUMERO FACTURA</td>");
        out.println("<td>ID PROVEEDOR</td>");
        out.println("<td>NOMBRE PROVEEDOR</td>");
        out.println("<td>ID PRODUCTO</td>");
        out.println("<td>NOMBRE PRODUCTO</td>");
        out.println("<td>CANTIDAD</td>");
        out.println("<td>PRECIO</td>");
        out.println("<td>IVA</td>");
        out.println("</tr>");

        ResultSet rs = objDat.consultar_DetalleCompra_Elim(id);

        while (rs.next()) {
            out.println("<tr>");
            out.println("<td>" + rs.getString("NUMERO_DETALLE_C") + "</td>");
            out.println("<td>" + rs.getString("NUM_FACTURA_C") + "</td>");
            out.println("<td>" + rs.getString("ID_PROVEEDOR") + "</td>");
            out.println("<td>" + rs.getString("NOMBRE_PROVE") + "</td>");
            out.println("<td>" + rs.getString("ID_PRODUCTO") + "</td>");
            out.println("<td>" + rs.getString("NOMBRE_PRO") + "</td>");
            out.println("<td>" + rs.getString("CANTIDAD_C") + "</td>");
            out.println("<td>" + rs.getString("PRECIO_C") + "</td>");
            out.println("<td>" + rs.getString("IVA_C") + "</td>");
            out.println("</tr>");

        }

        out.println("</table>");
        out.println("<br>");

        //CODIGO
        out.println(" <form action= ConfirEliminarDetalleFacturaCompras.do method=post>");
        out.println("<table >");
        out.println(" <tr><td>NUMERO DETALLE</td><td><input type=text name= Num_Detalle ></td></tr><br>");
        out.println(" </table>");
        out.println("<br>");

        out.println("<input class = boton type=submit name=Registrar value=Eliminar  style= 'text-align:center ;'>");
        out.println("</form>");

        //OTRO CODIGO TABLA
        out.println("<center>");
        out.println("<h1>Factura Compras </h1>");
        out.println("<table border='1'>");
        out.println("<tr>");
        out.println("<td>NUMERO FACTURA</td>");
        out.println("<td>ID PROVEEDOR</td>");
        out.println("<td>NOMBRE PROVEEDOR</td>");
        out.println("<td>DIRECCION PROVEEDOR</td>");
        out.println("</tr>");

        ResultSet rsa = objDat.consultar_FacturaCompra_Elim(id);
        while (rsa.next()) {
            out.println("<tr>");
            out.println("<td>" + rsa.getString("NUM_FACTURA_C") + "</td>");
            out.println("<td>" + rsa.getString("ID_PROVEEDOR") + "</td>");
            out.println("<td>" + rsa.getString("NOMBRE_PROVE") + "</td>");
            out.println("<td>" + rsa.getString("DIRECCION_PROVE") + "</td>");
            out.println("</tr>");

        }

        out.println("</table>");
        out.println("<br>");

        //CODIGO
        out.println(" <form action= ConfirEliminarFacturaCompras.do method=post>");
        out.println("<table >");
        out.println(" <tr><td>NUMERO FACTURA</td><td><input type=text name= Num_Factura ></td></tr><br>");
        out.println(" </table>");
        out.println("<br>");

        out.println("<input class = boton type=submit name=Registrar value=Eliminar  style= 'text-align:center ;'>");
        out.println("</form>");

        out.println("</center>");
        out.println("</body>");
        out.println("</html>");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(EliminarFacturaCompras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(EliminarFacturaCompras.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(EliminarFacturaCompras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(EliminarFacturaCompras.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
