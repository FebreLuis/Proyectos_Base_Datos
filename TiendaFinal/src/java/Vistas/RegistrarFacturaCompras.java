package Vistas;

import CLASES.FacturaCompras;
import CLASES.Productos;
import CLASES.Proveedor;

import Modelo.DAO;
import java.io.IOException;
import java.io.PrintWriter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author febre
 */
@WebServlet(urlPatterns = {"/RegistrarFacturaCompras.view"})
public class RegistrarFacturaCompras extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {

        try {

            response.setContentType("text/html;charset=UTF-8");

            PrintWriter out = response.getWriter();
            DAO objDao = new DAO();
            ArrayList<Proveedor> lista = objDao.Proveedor();
            ArrayList<Productos> lista3 = objDao.Productos();
            ArrayList<FacturaCompras> lista4 = objDao.FacturaCompras();
            

            try {
                /* TODO output your page here. You may use following sample code. */
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Registrar Factura Compras</title>");
                out.println("<link rel=stylesheet type=text/css href=tablain_1_1.css>");
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>Factura Compras</h1>");

                out.println("<a style='text-decoration:none; position: relative;' href='index.jsp'><b>INICIO</b></a>");
                out.println("<h3>Proveedores </h3>");
                out.println("<table border='1'>");
                out.println("<tr>");
                out.println("<td>ID PROVEEDOR</td>");
                out.println("<td>NOMBRE PROVEEDOR</td>");
                out.println("<td>DIRECCION PROVEEDOR</td>");

                out.println("</tr>");

                for (Proveedor w : lista) {
                    out.println("<tr>");
                    out.println("<td>" + w.getId_proveedor() + "</td>");
                    out.println("<td>" + w.getNombre_prove() + "</td>");
                    out.println("<td>" + w.getDireccion() + "</td>");
                    out.println("</tr>");
                }
                out.println("</table>");
                out.println("</div>");
                //otro

                out.println("<center>");
                out.println("<div class = re>");
                out.println(" <form action= ConfirRegistroFacturaCompras.do method=post>");
                out.println("<table align=center width=289 border=1 class=datos_form>");

                out.println("<tr><td>ID PROVEDDOR</td><td><input type=text name=ID_PROVEEDOR></td></tr><br>");
                out.println("<tr><td>FECHA</td><td><input type=text name=FECHA_CO></td></tr><br>");

                out.println("</table>");
                out.println("<br>");
                out.println("<input class = boton type=submit name=Registrar value=Registrar style='text-align:center;'> ");
                out.println("</form>");
                out.println("</center>");
                out.println("</div>");

                //AQUI EL CODIGO DATA PARA GUARDAR FACTURA
                //OTRO
                out.println("<h1>Detalle Compras</h1>");
                out.println("<div  class = mover2>");
                out.println("<h3>Facturas Compras Registradas</h3>");
                out.println("<table border='1'>");
                out.println("<tr>");
                out.println("<td>NUMERO FACTURA</td>");
                out.println("<td>ID PROVEEDOR</td>");
                out.println("<td>FECHA</td>");

                out.println("</tr>");

                for (FacturaCompras w : lista4) {
                    out.println("<tr>");
                    out.println("<td>" + w.getId_num_facturac() + "</td>");
                    out.println("<td>" + w.getId_proveedor() + "</td>");
                    out.println("<td>" + w.getFecha_Com() + "</td>");

                    out.println("</tr>");
                }

                out.println("</table>");

                out.println("</div>");

                //otra
                out.println("<div  class = mover>");
                out.println("<h3>Productos Comprar</h3>");
                out.println("<table border='1'>");
                out.println("<tr>");
                out.println("<td>ID PRODUCTO</td>");
                out.println("<td>ID CATEGORIA</td>");
                out.println("<td>NOMBRE PRODUCTO</td>");

                out.println("</tr>");

                for (Productos w : lista3) {
                    out.println("<tr>");
                    out.println("<td>" + w.getId_producto() + "</td>");
                    out.println("<td>" + w.getId_categoria() + "</td>");
                    out.println("<td>" + w.getNombre() + "</td>");

                    out.println("</tr>");
                }
                out.println("</table>");
                out.println("</div>");

                //boton
                out.println("<center>");
                out.println("<div class = re2>");
                out.println(" <form action= ConfirRegistroDetalleCompras.do method=post>");
                out.println("<table align=center width=289 border=1 class=datos_form>");

                out.println("<tr><td>NUMERO FACTURA</td><td><input type=text name=NUM_FACTURA_C></td></tr><br>");
                out.println("<tr><td>ID PRODUCTO</td><td><input type=text name=ID_PRODUCTO></td></tr><br>");
                out.println("<tr><td>CANTIDAD</td><td><input type=text name=CANTIDAD_C></td></tr><br>");
                out.println("<tr><td>PRECIO</td><td><input type=text name=PRECIO_C></td></tr><br>");
                out.println("<tr><td>IVA</td><td><input type=text name=IVA_C></td></tr><br>");

                out.println("</table>");
                out.println("<br>");
                out.println("<input class = boton type=submit name=Registrar value=Registrar style='text-align:center;'> ");
                out.println("</form>");
                out.println("</center>");
                out.println("</div>");

                out.println("</body>");
                out.println("</html>");
                out.println("</body>");
                out.println("</html>");
            } finally {
                out.close();
            }

        } catch (IOException ex) {
            Logger.getLogger(RegistrarFacturaCompras.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(RegistrarFacturaCompras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(RegistrarFacturaCompras.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(RegistrarFacturaCompras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(RegistrarFacturaCompras.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
