package Vistas;

import CLASES.Categoria;
import CLASES.Clientes;
import CLASES.Empleado;
import CLASES.Factura;
import CLASES.Productos;

import Modelo.DAO;
import java.io.IOException;
import java.io.PrintWriter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author febre
 */
@WebServlet(urlPatterns = {"/RegistrarFacturaVentas.view"})
public class RegistrarFacturaVentas extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {

        try {

            response.setContentType("text/html;charset=UTF-8");

            PrintWriter out = response.getWriter();
            DAO objDao = new DAO();
            ArrayList<Clientes> lista = objDao.Clientes();
            ArrayList<Empleado> lista2 = objDao.Empleado();
            ArrayList<Productos> lista3 = objDao.Productos();
            ArrayList<Factura> lista4 = objDao.Factura();

            try {
                /* TODO output your page here. You may use following sample code. */
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Registrar Factura Ventas</title>");
                out.println("<link rel=stylesheet type=text/css href=tablain_1.css>");
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>Factura Ventas</h1>");

                out.println("<a style='text-decoration:none; position: relative;' href='index.jsp'><b>INICIO</b></a>");
                out.println("<form action='ConsultarClientes.view' method='post'>");
                out.println("<div  class = mover2>");
                out.println("</form>");
                out.println("<h3>Clientes Registrados</h3>");
                out.println("<table border='1'>");
                out.println("<tr>");
                out.println("<td>ID CLIENTE</td>");
                out.println("<td>CEDULA</td>");
                out.println("<td>NOMBRES</td>");
                out.println("<td>TELEFONO</td>");
                out.println("<td>DIRECCION</td>");

                out.println("</tr>");

                for (Clientes w : lista) {
                    out.println("<tr>");
                    out.println("<td>" + w.getId_cliente() + "</td>");
                    out.println("<td>" + w.getCedula() + "</td>");
                    out.println("<td>" + w.getNombres() + "</td>");
                    out.println("<td>" + w.getTelefono() + "</td>");
                    out.println("<td>" + w.getDireccion() + "</td>");
                    out.println("</tr>");
                }
                out.println("</table>");
                out.println("</div>");
                //otro
                out.println("<div  class = mover>");
                out.println("<h3>Empleados Registrados</h3>");
                out.println("<table border='1'>");
                out.println("<tr>");
                out.println("<td>ID EMPLEADO</td>");
                out.println("<td>CODIGO EMPLEADO</td>");
                out.println("<td>NOMBRE</td>");
                out.println("<td>SUELDO</td>");
                out.println("<td>CARGO</td>");

                out.println("</tr>");

                for (Empleado w : lista2) {
                    out.println("<tr>");
                    out.println("<td>" + w.getId_emple() + "</td>");
                    out.println("<td>" + w.getCod_emple() + "</td>");
                    out.println("<td>" + w.getNombre() + "</td>");
                    out.println("<td>" + w.getSueldo() + "</td>");
                    out.println("<td>" + w.getCargo() + "</td>");

                    out.println("</tr>");
                }

                out.println("</table>");

                out.println("</div>");

                out.println("<center>");
                out.println("<div class = re>");
                out.println(" <form action= ConfirRegistroFacturaVentas.do method=post>");
                out.println("<table align=center width=289 border=1 class=datos_form>");

                out.println("<tr><td>ID CLIENTE</td><td><input type=text name=ID_CLIENTE></td></tr><br>");
                out.println("<tr><td>ID EMPLEADO</td><td><input type=text name=ID_EMPLE></td></tr><br>");
                out.println("<tr><td>FECHA</td><td><input type=text name=FECHA></td></tr><br>");

                out.println("</table>");
                out.println("<br>");
                out.println("<input class = boton type=submit name=Registrar value=Registrar style='text-align:center;'> ");
                out.println("</form>");
                out.println("</center>");
                out.println("</div>");

                //AQUI EL CODIGO DATA PARA GUARDAR FACTURA
                //OTRO
                out.println("<h1>Detalle Ventas</h1>");
                out.println("<div  class = mover2>");
                out.println("<h3>Facturas Registrados</h3>");
                out.println("<table border='1'>");
                out.println("<tr>");
                out.println("<td>NUMERO FACTURA</td>");
                out.println("<td>ID CLIENTE</td>");
                out.println("<td>ID EMPLEADO</td>");
                out.println("<td>FECHA</td>");

                out.println("</tr>");

                for (Factura w : lista4) {
                    out.println("<tr>");
                    out.println("<td>" + w.getNum_factura() + "</td>");
                    out.println("<td>" + w.getId_cliente() + "</td>");
                    out.println("<td>" + w.getId_empleado() + "</td>");
                    out.println("<td>" + w.getFecha() + "</td>");

                    out.println("</tr>");
                }

                out.println("</table>");

                out.println("</div>");

                //otra
                out.println("<div  class = mover>");
                out.println("<h3>ProductosRegistrados</h3>");
                out.println("<table border='1'>");
                out.println("<tr>");
                out.println("<td>ID PRODUCTO</td>");
                out.println("<td>ID CATEGORIA</td>");
                out.println("<td>NOMBRE PRODUCTO</td>");

                out.println("</tr>");

                for (Productos w : lista3) {
                    out.println("<tr>");
                    out.println("<td>" + w.getId_producto() + "</td>");
                    out.println("<td>" + w.getId_categoria() + "</td>");
                    out.println("<td>" + w.getNombre() + "</td>");

                    out.println("</tr>");
                }
                out.println("</table>");
                out.println("</div>");

                //boton
                out.println("<center>");
                out.println("<div class = re>");
                out.println(" <form action= ConfirRegistroDetalleVentas.do method=post>");
                out.println("<table align=center width=289 border=1 class=datos_form>");

                out.println("<tr><td>NUMERO FACTURA</td><td><input type=text name=NUM_FACTURA></td></tr><br>");
                out.println("<tr><td>ID PRODUCTO</td><td><input type=text name=ID_PRODUCTO></td></tr><br>");
                 out.println("<tr><td>CANTIDAD</td><td><input type=text name=CANTIDAD></td></tr><br>");
                out.println("<tr><td>PRECIO</td><td><input type=text name=PRECIO></td></tr><br>");
                out.println("<tr><td>IVA</td><td><input type=text name=IVA></td></tr><br>");
                

                out.println("</table>");
                out.println("<br>");
                out.println("<input class = boton type=submit name=Registrar value=Registrar style='text-align:center;'> ");
                out.println("</form>");
                out.println("</center>");
                out.println("</div>");

                out.println("</body>");
                out.println("</html>");
                out.println("</body>");
                out.println("</html>");
            } finally {
                out.close();
            }

        } catch (IOException ex) {
            Logger.getLogger(RegistrarFacturaVentas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(RegistrarFacturaVentas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(RegistrarFacturaVentas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(RegistrarFacturaVentas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(RegistrarFacturaVentas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
