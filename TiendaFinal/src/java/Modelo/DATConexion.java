package Modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Freddy
 */
public class DATConexion {
    public Connection con;//obj tipo Conecction

    public Connection getConnection() throws ClassNotFoundException, SQLException {
        String driver = "com.mysql.jdbc.Driver";
        String user = "root";
        String password = null;
        String url = "jdbc:mysql://localhost:3306/tiendafinal";
        Class.forName(driver);
        con = DriverManager.getConnection(url, user, password);
        return con;
    }

    public Connection AbrirConexion() throws ClassNotFoundException, SQLException {
        con = getConnection();
        return con;
    }

    public void CerrarConexion() throws SQLException {
        con.close();
    }
}
