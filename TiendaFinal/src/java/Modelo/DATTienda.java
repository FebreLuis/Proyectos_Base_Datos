package Modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author febre
 */
public class DATTienda {

    DATConexion cone = new DATConexion();

    //INGRESAR
    public int ingresarClientes(String cedula, String nombre, String telefono, String direccion) {
        int retorno = 0;
        String sentencia = "INSERT INTO cliente (CEDULA, NOMBRE_CLI, TELEFONO, DIRECCION) VALUES (?,?,?,?)";
        try {
            PreparedStatement pst = cone.AbrirConexion().prepareStatement(sentencia);
            pst.setString(1, cedula);
            pst.setString(2, nombre);
            pst.setString(3, telefono);
            pst.setString(4, direccion);
            retorno = pst.executeUpdate();

        } catch (Exception e) {
            System.out.println(e);
        }
        return retorno;
    }

    public int ingresarDetalle(int num_factura, int id_procducto, int cantidad, double precio, double iva) {
        int retorno = 0;
        String sentencia = "INSERT INTO detalle (NUM_FACTURA,ID_PRODUCTO, CANTIDAD,PRECIO, IVA) VALUES (?,?,?,?,?)";
        try {
            PreparedStatement pst = cone.AbrirConexion().prepareStatement(sentencia);

            pst.setInt(1, num_factura);
            pst.setInt(2, id_procducto);
            pst.setInt(3, cantidad);
            pst.setDouble(4, precio);
            pst.setDouble(5, iva);
            retorno = pst.executeUpdate();

        } catch (Exception e) {
            System.out.println(e);
        }
        return retorno;
    }

    public int ingresarDetalleCompras(int num_factura, int id_procducto, int cantidad, double precio, double iva) {
        int retorno = 0;
        String sentencia = "INSERT INTO detallecompra(NUM_FACTURA_C,ID_PRODUCTO, CANTIDAD_C,PRECIO_C, IVA_C) VALUES (?,?,?,?,?)";
        try {
            PreparedStatement pst = cone.AbrirConexion().prepareStatement(sentencia);

            pst.setInt(1, num_factura);
            pst.setInt(2, id_procducto);
            pst.setInt(3, cantidad);
            pst.setDouble(4, precio);
            pst.setDouble(5, iva);
            retorno = pst.executeUpdate();

        } catch (Exception e) {
            System.out.println(e);
        }
        return retorno;
    }

    public int ingresarProductos(int id_Cate, String nombre) {
        int retorno = 0;
        String sentencia = "INSERT INTO producto (ID_CATEGORIA,NOMBRE_PRO) VALUES (?,?)";
        try {
            PreparedStatement pst = cone.AbrirConexion().prepareStatement(sentencia);
            pst.setInt(1, id_Cate);
            pst.setString(2, nombre);

            retorno = pst.executeUpdate();

        } catch (Exception e) {
            System.out.println(e);
        }
        return retorno;
    }

    public int ingresarRFactura(int id_cliente, int id_emple, String fecha) {
        int retorno = 0;
        String sentencia = "INSERT INTO factura (ID_CLIENTE, ID_EMPLE, FECHA) VALUES (?,?,?)";
        try {
            PreparedStatement pst = cone.AbrirConexion().prepareStatement(sentencia);
            pst.setInt(1, id_cliente);
            pst.setInt(2, id_emple);
            pst.setString(3, fecha);

            retorno = pst.executeUpdate();

        } catch (Exception e) {
            System.out.println(e);
        }
        return retorno;
    }

    public int ingresarFacturaCompras(int id_proveedor, String fecha) {
        int retorno = 0;
        String sentencia = "INSERT INTO facturacompra (ID_PROVEEDOR , FECHA_CO) VALUES (?,?)";
        try {
            PreparedStatement pst = cone.AbrirConexion().prepareStatement(sentencia);
            pst.setInt(1, id_proveedor);
            pst.setString(2, fecha);

            retorno = pst.executeUpdate();

        } catch (Exception e) {
            System.out.println(e);
        }
        return retorno;
    }

    public int ingresarEmpleado(String cod_emple, String nombre, double sueldo, String cargo) {
        int retorno = 0;
        String sentencia = "INSERT INTO empleado (COD_EMPLE, NOMBRE_EMPLE, SUELDO, CARGO) VALUES (?,?,?,?)";
        try {
            PreparedStatement pst = cone.AbrirConexion().prepareStatement(sentencia);
            pst.setString(1, cod_emple);
            pst.setString(2, nombre);
            pst.setDouble(3, sueldo);
            pst.setString(4, cargo);

            retorno = pst.executeUpdate();

        } catch (Exception e) {
            System.out.println(e);
        }
        return retorno;
    }

    public int ingresarProveedor(String nombre_pro, String direccion) {
        int retorno = 0;
        String sentencia = "INSERT INTO proveedor (NOMBRE_PROVE, DIRECCION_PROVE) VALUES (?,?)";
        try {
            PreparedStatement pst = cone.AbrirConexion().prepareStatement(sentencia);
            pst.setString(1, nombre_pro);
            pst.setString(2, direccion);

            retorno = pst.executeUpdate();

        } catch (Exception e) {
            System.out.println(e);
        }
        return retorno;
    }

    public int ingresarCategoria(String nombre_cat, String descripcion) {
        int retorno = 0;
        String sentencia = "INSERT INTO categoria (NOMBRE_CATE, DESCRIPCION) VALUES (?,?)";
        try {
            PreparedStatement pst = cone.AbrirConexion().prepareStatement(sentencia);
            pst.setString(1, nombre_cat);
            pst.setString(2, descripcion);

            retorno = pst.executeUpdate();

        } catch (Exception e) {
            System.out.println(e);
        }
        return retorno;
    }

    //CONSULTAR
    public ResultSet consultar_Clientes() {
        ResultSet rs = null;
        try {
            PreparedStatement pre = cone.AbrirConexion().prepareStatement("SELECT * FROM cliente; ");
            rs = pre.executeQuery();//recupera un un ResultSet y envio la varible a executeQuery

        } catch (Exception e) {
            System.out.println(e);
        }
        return rs;
    }

    public ResultSet consultarFactura() {
        ResultSet rs = null;
        try {
            PreparedStatement pre = cone.AbrirConexion().prepareStatement("SELECT * FROM factura; ");
            rs = pre.executeQuery();//recupera un un ResultSet y envio la varible a executeQuery

        } catch (Exception e) {
            System.out.println(e);
        }
        return rs;
    }

    public ResultSet consultarFacturaCompra() {
        ResultSet rs = null;
        try {
            PreparedStatement pre = cone.AbrirConexion().prepareStatement("SELECT * FROM facturacompra; ");
            rs = pre.executeQuery();//recupera un un ResultSet y envio la varible a executeQuery

        } catch (Exception e) {
            System.out.println(e);
        }
        return rs;
    }

    public ResultSet consultarProveedor() {
        ResultSet rs = null;
        try {
            PreparedStatement pre = cone.AbrirConexion().prepareStatement("SELECT * FROM proveedor; ");
            rs = pre.executeQuery();//recupera un un ResultSet y envio la varible a executeQuery

        } catch (Exception e) {
            System.out.println(e);
        }
        return rs;
    }

    public ResultSet consultarCategoria() {
        ResultSet rs = null;
        try {
            PreparedStatement pre = cone.AbrirConexion().prepareStatement("SELECT * FROM categoria; ");
            rs = pre.executeQuery();//recupera un un ResultSet y envio la varible a executeQuery

        } catch (Exception e) {
            System.out.println(e);
        }
        return rs;
    }

    public ResultSet consultar_Productos() {
        ResultSet rs = null;
        try {
            PreparedStatement pre = cone.AbrirConexion().prepareStatement("SELECT * FROM producto; ");
            rs = pre.executeQuery();//recupera un un ResultSet y envio la varible a executeQuery

        } catch (Exception e) {
            System.out.println(e);
        }
        return rs;
    }

    //MODIFICADO HASTA AQUI
    public ResultSet consultar_Factura_Cliente(String cedula) {
        ResultSet rs = null;
        try {
            PreparedStatement pre = cone.AbrirConexion().prepareStatement("SELECT cliente.CEDULA, cliente.NOMBRE_CLI, cliente.DIRECCION, empleado.NOMBRE_EMPLE,factura.NUM_FACTURA,factura.FECHA, detalle.NUMERO_DETALLE, producto.NOMBRE_PRO,categoria.NOMBRE_CATE,detalle.CANTIDAD,detalle.PRECIO,detalle.IVA, ((detalle.CANTIDAD*detalle.PRECIO)*detalle.IVA + detalle.CANTIDAD * detalle.PRECIO) AS TOTAL FROM cliente, empleado,detalle,factura,producto,categoria WHERE (producto.ID_PRODUCTO = detalle.ID_PRODUCTO) AND(cliente.ID_CLIENTE = factura.ID_CLIENTE) AND(empleado.ID_EMPLE = factura.ID_EMPLE) And(categoria.ID_CATEGORIA = producto.ID_CATEGORIA) AND (factura.NUM_FACTURA = detalle.NUM_FACTURA AND cliente.CEDULA ='"+cedula+"');");
            rs = pre.executeQuery();//recupera un un ResultSet y envio la varible a executeQuery

        } catch (Exception e) {
            System.out.println(e);
        }
        return rs;//retornma en un obj ResultSet rs
    }

    public ResultSet consultar_FacturaCompra_Provee(String proveedor) {
        ResultSet rs = null;
        try {
            PreparedStatement pre = cone.AbrirConexion().prepareStatement("select proveedor.NOMBRE_PROVE , proveedor.DIRECCION_PROVE , facturacompra.NUM_FACTURA_C , facturacompra.FECHA_CO,detallecompra.NUMERO_DETALLE_C,producto.NOMBRE_PRO, detallecompra.CANTIDAD_C ,detallecompra.PRECIO_C , detallecompra.IVA_C ,((detallecompra.CANTIDAD_C*detallecompra.PRECIO_C)*detallecompra.IVA_C + detallecompra.CANTIDAD_C * detallecompra.PRECIO_C) AS TOTAL from producto , proveedor , facturacompra , detallecompra WHERE (producto.ID_PRODUCTO = detallecompra.ID_PRODUCTO) AND(proveedor.ID_PROVEEDOR = facturacompra.ID_PROVEEDOR) AND (facturacompra.NUM_FACTURA_C = detallecompra.NUM_FACTURA_C AND proveedor.NOMBRE_PROVE = '" + proveedor + "');");
            rs = pre.executeQuery();//recupera un un ResultSet y envio la varible a executeQuery

        } catch (Exception e) {
            System.out.println(e);
        }
        return rs;//retornma en un obj ResultSet rs
    }

    public ResultSet consultar_Empleado_Clientes(int Empleado) {
        ResultSet rs = null;
        try {
            PreparedStatement pre = cone.AbrirConexion().prepareStatement("SELECT  cliente.CEDULA,cliente.NOMBRE_CLI  as CLIENTES_ATENDIDOS FROM empleado , factura,detalle,cliente where (factura.ID_CLIENTE = cliente.ID_CLIENTE) and(empleado.ID_EMPLE = factura.ID_EMPLE)and(detalle.NUM_FACTURA = factura.NUM_FACTURA)and (empleado.ID_EMPLE =" + Empleado + ") group by cliente.NOMBRE_CLI ;");
            rs = pre.executeQuery();//recupera un un ResultSet y envio la varible a executeQuery

        } catch (Exception e) {
            System.out.println(e);
        }
        return rs;//retornma en un obj ResultSet rs
    }

    public ResultSet consultar_Detalle_Elim(String cliente) {
        ResultSet rs = null;
        try {
            PreparedStatement pre = cone.AbrirConexion().prepareStatement("select detalle.NUMERO_DETALLE , detalle.NUM_FACTURA,cliente.ID_CLIENTE, cliente.NOMBRE_CLI , detalle.ID_PRODUCTO, producto.NOMBRE_PRO ,detalle.CANTIDAD , detalle.PRECIO,detalle.IVA from detalle ,factura,cliente,producto where (factura.ID_CLIENTE  = cliente.ID_CLIENTE) and(detalle.NUM_FACTURA = factura.NUM_FACTURA) and (producto.ID_PRODUCTO = detalle.ID_PRODUCTO) and(cliente.CEDULA = '" + cliente + "');");
            rs = pre.executeQuery();//recupera un un ResultSet y envio la varible a executeQuery

        } catch (Exception e) {
            System.out.println(e);
        }
        return rs;//retornma en un obj ResultSet rs
    }

    public ResultSet consultar_DetalleCompra_Elim(String proveedor) {
        ResultSet rs = null;
        try {
            PreparedStatement pre = cone.AbrirConexion().prepareStatement("select detallecompra.NUMERO_DETALLE_C , detallecompra.NUM_FACTURA_C, proveedor.ID_PROVEEDOR, proveedor.NOMBRE_PROVE , detallecompra.ID_PRODUCTO , producto.NOMBRE_PRO ,detallecompra.CANTIDAD_C , detallecompra.PRECIO_C,detallecompra.IVA_C from detallecompra ,facturacompra,proveedor,producto where (facturacompra.ID_PROVEEDOR  = proveedor.ID_PROVEEDOR) and(detallecompra.NUM_FACTURA_C = facturacompra.NUM_FACTURA_C) and (producto.ID_PRODUCTO = detallecompra.ID_PRODUCTO) and(proveedor.NOMBRE_PROVE = '" + proveedor + "');");
            rs = pre.executeQuery();//recupera un un ResultSet y envio la varible a executeQuery

        } catch (Exception e) {
            System.out.println(e);
        }
        return rs;//retornma en un obj ResultSet rs
    }

    public ResultSet consultar_Factura_Elim(String cliente) {
        ResultSet rs = null;
        try {
            PreparedStatement pre = cone.AbrirConexion().prepareStatement("select factura.NUM_FACTURA , cliente.ID_CLIENTE, cliente.NOMBRE_CLI , cliente.CEDULA  from cliente , factura  where (factura.ID_CLIENTE = cliente.ID_CLIENTE) and cliente.CEDULA = '" + cliente + "';");
            rs = pre.executeQuery();//recupera un un ResultSet y envio la varible a executeQuery

        } catch (Exception e) {
            System.out.println(e);
        }
        return rs;//retornma en un obj ResultSet rs
    }

    public ResultSet consultar_FacturaCompra_Elim(String proveedor) {
        ResultSet rs = null;
        try {
            PreparedStatement pre = cone.AbrirConexion().prepareStatement("select facturacompra.NUM_FACTURA_C , proveedor.ID_PROVEEDOR, proveedor.NOMBRE_PROVE , proveedor.DIRECCION_PROVE  from facturacompra , proveedor where (facturacompra.ID_PROVEEDOR = proveedor.ID_PROVEEDOR) and proveedor.NOMBRE_PROVE = '" + proveedor + "';");
            rs = pre.executeQuery();//recupera un un ResultSet y envio la varible a executeQuery

        } catch (Exception e) {
            System.out.println(e);
        }
        return rs;//retornma en un obj ResultSet rs
    }

    public ResultSet consultar_Empleado() {
        ResultSet rs = null;
        try {
            PreparedStatement pre = cone.AbrirConexion().prepareStatement("SELECT * FROM empleado ; ");
            rs = pre.executeQuery();//recupera un un ResultSet y envio la varible a executeQuery

        } catch (Exception e) {
            System.out.println(e);
        }
        return rs;//retornma en un obj ResultSet rs
    }

    //MODIFICAR AQUI
    public int modificarClientes(String nombre, String telefono, String direccion, int id) {
        int retorno = 0;

        String sentencia = ("update cliente set NOMBRE_CLI=?, TELEFONO=?, DIRECCION=? where ID_CLIENTE =?");
        try {

            PreparedStatement pst = cone.AbrirConexion().prepareStatement(sentencia);
            pst.setString(1, nombre);
            pst.setString(2, telefono);
            pst.setString(3, direccion);
            pst.setInt(4, id);
            retorno = pst.executeUpdate();
            pst.close();

        } catch (Exception e) {
        }
        return retorno;
    }
    
    
     public int modificarProveedor(String nombre, String direccion, int id) {
        int retorno = 0;

        String sentencia = ("update proveedor set NOMBRE_PROVE=?, DIRECCION_PROVE=? where ID_PROVEEDOR =?");
        try {

            PreparedStatement pst = cone.AbrirConexion().prepareStatement(sentencia);
            pst.setString(1, nombre);
            pst.setString(2, direccion);
            pst.setInt(3, id);
            retorno = pst.executeUpdate();
            pst.close();

        } catch (Exception e) {
        }
        return retorno;
    }

    public int modificarProductos(String nombre, int id) {
        int retorno = 0;

        String sentencia = "UPDATE producto SET NOMBRE_PRO = '" + nombre + "'" + " WHERE ID_PRODUCTO = ?";
        try {

            PreparedStatement pst = cone.AbrirConexion().prepareStatement(sentencia);
            pst.setInt(1, id);
            retorno = pst.executeUpdate();
            pst.close();

        } catch (Exception e) {
        }
        return retorno;
    }

    public int ModificarEmpleado(String cod_emple, String nombre, double sueldo, String cargo, int id) {
        int retorno = 0;

        String sentencia = "update empleado set COD_EMPLE=?, NOMBRE_EMPLE=?, SUELDO=? ,CARGO =? where ID_EMPLE =?";
        try {

            PreparedStatement pst = cone.AbrirConexion().prepareStatement(sentencia);
            pst.setString(1, cod_emple);
            pst.setString(2, nombre);
            pst.setDouble(3, sueldo);
            pst.setString(4, cargo);
            pst.setInt(5, id);
            retorno = pst.executeUpdate();
            pst.close();

        } catch (Exception e) {
        }
        return retorno;
    }

    public int ModificarCategoria(String nombre_cate, String descripcion, int id) {
        int retorno = 0;

        String sentencia = "update categoria set NOMBRE_CATE=?, DESCRIPCION =? where ID_CATEGORIA =?";
        try {

            PreparedStatement pst = cone.AbrirConexion().prepareStatement(sentencia);
            pst.setString(1, nombre_cate);
            pst.setString(2, descripcion);
            pst.setInt(3, id);
            retorno = pst.executeUpdate();
            pst.close();

        } catch (Exception e) {
        }
        return retorno;
    }

    //ELIMINAR
    public int eliminarCliente(int id_cliente) {
        int retorno = 0;
        String sentencia = "DELETE FROM cliente WHERE ID_CLIENTE= ?";
        try {
            PreparedStatement pst = cone.AbrirConexion().prepareStatement(sentencia);
            pst.setInt(1, id_cliente);
            retorno = pst.executeUpdate();
            pst.close();
        } catch (Exception e) {

        }
        return retorno;
    }

    public int eliminarProductos(int id_produc) {
        int retorno = 0;
        String sentencia = "DELETE FROM producto WHERE ID_PRODUCTO= ?";
        try {
            PreparedStatement pst = cone.AbrirConexion().prepareStatement(sentencia);
            pst.setInt(1, id_produc);
            retorno = pst.executeUpdate();
            pst.close();
        } catch (Exception e) {

        }
        return retorno;
    }

    public int eliminarDepartamento(int id_depar) {
        int retorno = 0;
        String sentencia = "DELETE FROM departamento WHERE ID_DEPAR= ?";
        try {
            PreparedStatement pst = cone.AbrirConexion().prepareStatement(sentencia);
            pst.setInt(1, id_depar);
            retorno = pst.executeUpdate();
            pst.close();
        } catch (Exception e) {

        }
        return retorno;
    }

    public int eliminarEmpleado(int id_emple) {
        int retorno = 0;
        String sentencia = "DELETE FROM empleado WHERE ID_EMPLE= ?";
        try {
            PreparedStatement pst = cone.AbrirConexion().prepareStatement(sentencia);
            pst.setInt(1, id_emple);
            retorno = pst.executeUpdate();
            pst.close();
        } catch (Exception e) {

        }
        return retorno;
    }

    //Eliminar factura
    public int eliminarDetalle(int id_detalle) {
        int retorno = 0;
        String sentencia = "DELETE FROM detalle WHERE NUMERO_DETALLE= ?";
        try {
            PreparedStatement pst = cone.AbrirConexion().prepareStatement(sentencia);
            pst.setInt(1, id_detalle);
            retorno = pst.executeUpdate();
            pst.close();
        } catch (Exception e) {

        }
        return retorno;
    }

    public int eliminarFactura(int id_detalle) {
        int retorno = 0;
        String sentencia = "DELETE FROM factura WHERE NUM_FACTURA= ?";
        try {
            PreparedStatement pst = cone.AbrirConexion().prepareStatement(sentencia);
            pst.setInt(1, id_detalle);
            retorno = pst.executeUpdate();
            pst.close();
        } catch (Exception e) {

        }
        return retorno;
    }

    public int eliminarDetalleCompras(int id_detalle) {
        int retorno = 0;
        String sentencia = "DELETE FROM detallecompra WHERE NUMERO_DETALLE_C= ?";
        try {
            PreparedStatement pst = cone.AbrirConexion().prepareStatement(sentencia);
            pst.setInt(1, id_detalle);
            retorno = pst.executeUpdate();
            pst.close();
        } catch (Exception e) {

        }
        return retorno;
    }

    public int eliminarFacturaCompras(int id_detalle) {
        int retorno = 0;
        String sentencia = "DELETE FROM facturacompra WHERE NUM_FACTURA_C = ?";
        try {
            PreparedStatement pst = cone.AbrirConexion().prepareStatement(sentencia);
            pst.setInt(1, id_detalle);
            retorno = pst.executeUpdate();
            pst.close();
        } catch (Exception e) {

        }
        return retorno;
    }

}
