package Modelo;

import CLASES.Categoria;
import CLASES.Clientes;
import CLASES.Empleado;
import CLASES.Factura;
import CLASES.FacturaCompras;
import CLASES.FacturaVi;
import CLASES.Productos;
import CLASES.Proveedor;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

/**
 *
 * @author febre
 */
@WebServlet(name = "DAO", urlPatterns = {"/DAO.do"})
public class DAO extends HttpServlet {

    DATTienda objTi = new DATTienda();

    public ArrayList<Clientes> Clientes() throws ClassNotFoundException, SQLException {
        ArrayList<Clientes> lista = new ArrayList<>();
        ResultSet rs = objTi.consultar_Clientes();
        while (rs.next()) {
            String id = rs.getString("ID_CLIENTE");
            String cedula = rs.getString("CEDULA");
            String nombre = rs.getString("NOMBRE_CLI");
            String telefono = rs.getString("TELEFONO");
            String direccion = rs.getString("DIRECCION");
            int tras1 = Integer.parseInt(id);

            Clientes objObjeto = new Clientes(tras1, cedula, nombre, telefono, direccion);
            lista.add(objObjeto);
        }

        return lista;
    }

    public ArrayList<Productos> Productos() throws ClassNotFoundException, SQLException {
        ArrayList<Productos> lista = new ArrayList<>();
        ResultSet rs = objTi.consultar_Productos();
        while (rs.next()) {
            String id = rs.getString("ID_PRODUCTO");
            String cod = rs.getString("ID_CATEGORIA");
            String nombre = rs.getString("NOMBRE_PRO");

            int tras1 = Integer.parseInt(id);
            int tras2 = Integer.parseInt(cod);

            Productos objObjeto = new Productos(tras1, tras2, nombre);
            lista.add(objObjeto);
        }

        return lista;
    }

    public ArrayList<Empleado> Empleado() throws ClassNotFoundException, SQLException {
        ArrayList<Empleado> lista = new ArrayList<>();
        ResultSet rs = objTi.consultar_Empleado();
        while (rs.next()) {
            String id = rs.getString("ID_EMPLE");
            String cod = rs.getString("COD_EMPLE");
            String nombre = rs.getString("NOMBRE_EMPLE");
            String sueldo = rs.getString("SUELDO");
            String cargo = rs.getString("CARGO");

            int tras1 = Integer.parseInt(id);
            double tras2 = Double.parseDouble(sueldo);
            Empleado objObjeto = new Empleado(tras1, cod, nombre, tras2, cargo);
            lista.add(objObjeto);
        }

        return lista;
    }

    public ArrayList<Categoria> Categoria() throws ClassNotFoundException, SQLException {
        ArrayList<Categoria> lista = new ArrayList<>();
        ResultSet rs = objTi.consultarCategoria();
        while (rs.next()) {
            String id = rs.getString("ID_CATEGORIA");
            String nom_cate = rs.getString("NOMBRE_CATE");
            String descr = rs.getString("DESCRIPCION");

            int tras1 = Integer.parseInt(id);

            Categoria objObjeto = new Categoria(tras1, nom_cate, descr);
            lista.add(objObjeto);
        }

        return lista;
    }

    public ArrayList<Factura> Factura() throws ClassNotFoundException, SQLException {
        ArrayList<Factura> lista = new ArrayList<>();
        ResultSet rs = objTi.consultarFactura();
        while (rs.next()) {
            String id_fac = rs.getString("NUM_FACTURA");
            String id_cli = rs.getString("ID_CLIENTE");
            String id_emple = rs.getString("ID_EMPLE");
            String fecha = rs.getString("FECHA");

            int tras1 = Integer.parseInt(id_fac);
            int tras2 = Integer.parseInt(id_cli);
            int tras3 = Integer.parseInt(id_emple);
            Factura objObjeto = new Factura(tras1, tras2, tras3, fecha);
            lista.add(objObjeto);
        }

        return lista;
    }

    public ArrayList<Proveedor> Proveedor() throws ClassNotFoundException, SQLException {
        ArrayList<Proveedor> lista = new ArrayList<>();
        ResultSet rs = objTi.consultarProveedor();
        while (rs.next()) {
            String id_pro = rs.getString("ID_PROVEEDOR");
            String nombre_prov = rs.getString("NOMBRE_PROVE");
            String direccion_pro = rs.getString("DIRECCION_PROVE");

            int tras1 = Integer.parseInt(id_pro);
            Proveedor objObjeto = new Proveedor(tras1, nombre_prov, direccion_pro);
            lista.add(objObjeto);
        }

        return lista;
    }

    public ArrayList<FacturaCompras> FacturaCompras() throws ClassNotFoundException, SQLException {
        ArrayList<FacturaCompras> lista = new ArrayList<>();
        ResultSet rs = objTi.consultarFacturaCompra();
        while (rs.next()) {
            String id_fac = rs.getString("NUM_FACTURA_C");
            String id_pro = rs.getString("ID_PROVEEDOR");
            String fecha = rs.getString("FECHA_CO");

            int tras1 = Integer.parseInt(id_fac);
            int tras2 = Integer.parseInt(id_pro);

            FacturaCompras objObjeto = new FacturaCompras(tras1, tras2, fecha);
            lista.add(objObjeto);
        }

        return lista;
    }

}
