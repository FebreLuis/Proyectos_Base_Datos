package CLASES;

/**
 *
 * @author Febre
 */
public class Categoria {

    private int id_categoria;
    private String nombre_categ;
    private String descripcion;

    //Constrcutor
    public Categoria(int id_categoria, String nombre_categ, String descripcion) {
        this.id_categoria = id_categoria;
        this.nombre_categ = nombre_categ;
        this.descripcion = descripcion;
    }

    //Metodos
    public int getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(int id_categoria) {
        this.id_categoria = id_categoria;
    }

    public String getNombre_categ() {
        return nombre_categ;
    }

    public void setNombre_categ(String nombre_categ) {
        this.nombre_categ = nombre_categ;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
