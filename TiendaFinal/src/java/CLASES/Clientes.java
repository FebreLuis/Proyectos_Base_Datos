package CLASES;

/**
 *
 * @author febre
 */
public class Clientes {

    private int id_cliente;
    private String cedula;
    private String nombres;
    private String telefono;
    private String direccion;

    //Constructor
    public Clientes(int id_cliente, String cedula, String nombres, String telefono, String direccion) {
        this.id_cliente = id_cliente;
        this.cedula = cedula;
        this.nombres = nombres;
        this.telefono = telefono;
        this.direccion = direccion;
    }

    //Metodos
    
    public int getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

}
