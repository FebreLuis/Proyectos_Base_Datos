package CLASES;

/**
 *
 * @author Febre
 */
public class FacturaCompras {

    private int id_num_facturac;
    private int id_proveedor;
    private String fecha_Com;

    //Constructor
    public FacturaCompras(int id_num_facturac, int id_proveedor, String fecha_Com) {
        this.id_num_facturac = id_num_facturac;
        this.id_proveedor = id_proveedor;
        this.fecha_Com = fecha_Com;
    }

    //Metodos
    public int getId_num_facturac() {
        return id_num_facturac;
    }

    public void setId_num_facturac(int id_num_facturac) {
        this.id_num_facturac = id_num_facturac;
    }

    public int getId_proveedor() {
        return id_proveedor;
    }

    public void setId_proveedor(int id_proveedor) {
        this.id_proveedor = id_proveedor;
    }

    public String getFecha_Com() {
        return fecha_Com;
    }

    public void setFecha_Com(String fecha_Com) {
        this.fecha_Com = fecha_Com;
    }

}
