package CLASES;

/**
 *
 * @author Febre
 */
public class FacturaVi {

    private String cedula;
    private String nombre;
    private String direccion;
    private int numFactura;
    private String fecha;
    private int cantidad;
    private String nombrePro;
    private double precio;
    private double total;

    //Costructor
    public FacturaVi(String cedula, String nombre, String direccion, int numFactura, String fecha, int cantidad, String nombrePro, double precio, double total) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.direccion = direccion;
        this.numFactura = numFactura;
        this.fecha = fecha;
        this.cantidad = cantidad;
        this.nombrePro = nombrePro;
        this.precio = precio;
        this.total = total;
    }

    //Metodos
    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getNumFactura() {
        return numFactura;
    }

    public void setNumFactura(int numFactura) {
        this.numFactura = numFactura;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getNombrePro() {
        return nombrePro;
    }

    public void setNombrePro(String nombrePro) {
        this.nombrePro = nombrePro;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

}
