package CLASES;

/**
 *
 * @author Febre
 */
public class Proveedor {

    private int id_proveedor;
    private String nombre_prove;
    private String direccion;

    //Constrcutor
    public Proveedor(int id_proveedor, String nombre_prove, String direccion) {
        this.id_proveedor = id_proveedor;
        this.nombre_prove = nombre_prove;
        this.direccion = direccion;
    }

    //Metodos
    public int getId_proveedor() {
        return id_proveedor;
    }

    public void setId_proveedor(int id_proveedor) {
        this.id_proveedor = id_proveedor;
    }

    public String getNombre_prove() {
        return nombre_prove;
    }

    public void setNombre_prove(String nombre_prove) {
        this.nombre_prove = nombre_prove;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

}
