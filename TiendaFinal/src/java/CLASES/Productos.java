package CLASES;

/**
 *
 * @author febre
 */
public class Productos {

    private int id_producto;
    private int id_categoria;
    private String nombre;

    //Constructor
    public Productos(int id_producto, int id_categoria, String nombre) {
        this.id_producto = id_producto;
        this.id_categoria = id_categoria;
        this.nombre = nombre;
    }

    //Metodos
    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public int getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(int id_categoria) {
        this.id_categoria = id_categoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
