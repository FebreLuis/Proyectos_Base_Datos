package CLASES;

/**
 *
 * @author febre
 */
public class Producto_Empleado {

    private int f_id_empleado;
    private int f_id_producto;

    //Constructor
    public Producto_Empleado(int f_id_empleado, int f_id_producto) {
        this.f_id_empleado = f_id_empleado;
        this.f_id_producto = f_id_producto;
    }

    //Metodos
    public int getF_id_empleado() {
        return f_id_empleado;
    }

    public void setF_id_empleado(int f_id_empleado) {
        this.f_id_empleado = f_id_empleado;
    }

    public int getF_id_producto() {
        return f_id_producto;
    }

    public void setF_id_producto(int f_id_producto) {
        this.f_id_producto = f_id_producto;
    }

}
