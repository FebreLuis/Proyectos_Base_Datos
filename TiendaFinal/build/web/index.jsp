<%-- 
    Document   : index
    Created on : 05/11/2017, 18:42:35
    Author     : Febre
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="estilo.css">

        <title>Tienda Factura</title>
    </head>
    <body  >

        <div class = "main">

            <center>
                <h1 class="titulo" >Bienvenido "Programa Tienda"</h1>

                <div class="uno">
                    <h2 >Clientes </h2>
                    <nav>
                        <a href="Registrar_Cliente.jsp" >Registrar Cliente</a><br>
                        <a href="ConsultarClientes.view" >Clientes Registrados</a><br>
                        <a href="Modificar_Cliente.do" >Modificar Clientes </a><br>

                    </nav>
                </div>

                <div class="dos">
                    <h2 >Categoria </h2>
                    <nav>
                        <a href="Registrar_Categoria.jsp" >Registrar Categoria</a><br>
                        <a href="ConsultarCategoria.view">Categorias Registradas</a><br>
                        <a href="Modificar_Categoria.do">Modificar Categorias</a><br>
                        


                    </nav>
                </div>



                <div class="tres">
                    <h2 >Proveedor </h2>
                    <nav>
                       
                         <a href=" Registrar_Proveedor.jsp" >Registrar Proveedor</a><br>
                        <a href="ConsultarProveedor.view" >Proveedores Registrados</a><br>
                        <a href="Modificar_Proveedor.do" >Modificar Proveedores</a><br>
                        

                    </nav>
                </div>

                <div class="cuatro">
                    <h2 >Productos </h2>
                    <nav>
                        <a href="RegistrarProducto.view" >Registrar Productos</a><br>
                        <a href="ConsultarProductos.view" >Productos Registrados</a><br>
                        <a href="Modificar_Productos.do" >Modificar Productos </a><br>

                    </nav>
                </div>



                <div class="cinco">
                    <h2 >Empleados </h2>
                    <nav>
                        <a href="Registrar_Empleado.jsp" >Registrar Empleados</a><br>
                        <a href="ConsultarEmpleados.view" >Empleados Registrados</a><br>
                        <a href="Modificar_Empleado.do" >Modificar Empleados </a><br>

                    </nav>
                </div>


                <div class="seis">
                    <h2 >Factura Ventas </h2>
                    <nav>
                        <a href="RegistrarFacturaVentas.view" >Registrar Factura Ventas</a><br>
                        <a href="EliminarFacturaVentas.do" >Eliminar Factura Ventas </a><br>


                    </nav>
                </div>


                <div class="siete">
                    <h2 >Factura Compras </h2>
                    <nav>
                        <a href="RegistrarFacturaCompras.view" >Registrar Factura Compras</a><br>
                        <a href="EliminarFacturaCompras.do" style="text-decoration: none">Eliminar Factura Compras </a><br>
                    </nav>
                </div>



                <div class="ocho">
                    <h2 >Factura-Empleado</h2>

                    <nav>
                        <a href="ConsultaFacturaVentasCliente.do" >Factura Ventas</a><br>
                        <a href="ConsultaFacturaComprasProveedor.do" >Factura Compras</a><br>
                        <a href="ConsultaReleacionEmpleadoCliente.do" >Cliente-Empleados </a><br>

                    </nav>
                </div>
            </center>

        </div>

    </body>
</html>
