package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta charset=\"utf-8\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"estilo.css\">\n");
      out.write("\n");
      out.write("        <title>Tienda Factura</title>\n");
      out.write("    </head>\n");
      out.write("    <body  >\n");
      out.write("\n");
      out.write("        <div class = \"main\">\n");
      out.write("\n");
      out.write("            <center>\n");
      out.write("                <h1 class=\"titulo\" >Bienvenido \"Programa Tienda\"</h1>\n");
      out.write("\n");
      out.write("                <div class=\"uno\">\n");
      out.write("                    <h2 >Clientes </h2>\n");
      out.write("                    <nav>\n");
      out.write("                        <a href=\"Registrar_Cliente.jsp\" >Registrar Cliente</a><br>\n");
      out.write("                        <a href=\"ConsultarClientes.view\" >Clientes Registrados</a><br>\n");
      out.write("                        <a href=\"Modificar_Cliente.do\" >Modificar Clientes </a><br>\n");
      out.write("\n");
      out.write("                    </nav>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("                <div class=\"dos\">\n");
      out.write("                    <h2 >Categoria </h2>\n");
      out.write("                    <nav>\n");
      out.write("                        <a href=\"Registrar_Categoria.jsp\" >Registrar Categoria</a><br>\n");
      out.write("                        <a href=\"ConsultarCategoria.view\">Categorias Registradas</a><br>\n");
      out.write("                        <a href=\"Modificar_Categoria.do\">Modificar Categorias</a><br>\n");
      out.write("                        \n");
      out.write("\n");
      out.write("\n");
      out.write("                    </nav>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("                <div class=\"tres\">\n");
      out.write("                    <h2 >Proveedor </h2>\n");
      out.write("                    <nav>\n");
      out.write("                       \n");
      out.write("                         <a href=\" Registrar_Proveedor.jsp\" >Registrar Proveedor</a><br>\n");
      out.write("                        <a href=\"ConsultarProveedor.view\" >Proveedores Registrados</a><br>\n");
      out.write("                        <a href=\"Modificar_Proveedor.do\" >Modificar Proveedores</a><br>\n");
      out.write("                        \n");
      out.write("\n");
      out.write("                    </nav>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("                <div class=\"cuatro\">\n");
      out.write("                    <h2 >Productos </h2>\n");
      out.write("                    <nav>\n");
      out.write("                        <a href=\"RegistrarProducto.view\" >Registrar Productos</a><br>\n");
      out.write("                        <a href=\"ConsultarProductos.view\" >Productos Registrados</a><br>\n");
      out.write("                        <a href=\"Modificar_Productos.do\" >Modificar Productos </a><br>\n");
      out.write("\n");
      out.write("                    </nav>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("                <div class=\"cinco\">\n");
      out.write("                    <h2 >Empleados </h2>\n");
      out.write("                    <nav>\n");
      out.write("                        <a href=\"Registrar_Empleado.jsp\" >Registrar Empleados</a><br>\n");
      out.write("                        <a href=\"ConsultarEmpleados.view\" >Empleados Registrados</a><br>\n");
      out.write("                        <a href=\"Modificar_Empleado.do\" >Modificar Empleados </a><br>\n");
      out.write("\n");
      out.write("                    </nav>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("                <div class=\"seis\">\n");
      out.write("                    <h2 >Factura Ventas </h2>\n");
      out.write("                    <nav>\n");
      out.write("                        <a href=\"RegistrarFacturaVentas.view\" >Registrar Factura Ventas</a><br>\n");
      out.write("                        <a href=\"EliminarFacturaVentas.do\" >Eliminar Factura Ventas </a><br>\n");
      out.write("\n");
      out.write("\n");
      out.write("                    </nav>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("                <div class=\"siete\">\n");
      out.write("                    <h2 >Factura Compras </h2>\n");
      out.write("                    <nav>\n");
      out.write("                        <a href=\"RegistrarFacturaCompras.view\" >Registrar Factura Compras</a><br>\n");
      out.write("                        <a href=\"EliminarFacturaCompras.do\" style=\"text-decoration: none\">Eliminar Factura Compras </a><br>\n");
      out.write("                    </nav>\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("                <div class=\"ocho\">\n");
      out.write("                    <h2 >Factura-Empleado</h2>\n");
      out.write("\n");
      out.write("                    <nav>\n");
      out.write("                        <a href=\"ConsultaFacturaVentasCliente.do\" >Factura Ventas</a><br>\n");
      out.write("                        <a href=\"ConsultaFacturaComprasProveedor.do\" >Factura Compras</a><br>\n");
      out.write("                        <a href=\"ConsultaReleacionEmpleadoCliente.do\" >Cliente-Empleados </a><br>\n");
      out.write("\n");
      out.write("                    </nav>\n");
      out.write("                </div>\n");
      out.write("            </center>\n");
      out.write("\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
