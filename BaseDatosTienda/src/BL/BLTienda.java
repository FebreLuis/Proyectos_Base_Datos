package BL;

import CLASES.Categoria;
import CLASES.Clientes;
import CLASES.Detalle;
import CLASES.DetalleCompras;

import CLASES.Empleado;
import CLASES.Factura;
import CLASES.FacturaCompras;
import CLASES.Productos;
import CLASES.Proveedor;
import DAT.DATTienda;
import java.sql.ResultSet;

/**
 *
 * @author febre
 */
public class BLTienda {

    DATTienda objDat = new DATTienda();

    //INGRESAR
    public int ingresarClientes(Clientes objCli) {

        return objDat.ingresarClientes(objCli.getCedula(), objCli.getNombres(), objCli.getTelefono(), objCli.getDireccion());

    }

    public int ingresarDetalle(int a, int b, int c, double d, double e) {

        return objDat.ingresarDetalle(a, b, c, d, e);

    }

    public int ingresarDetalle2(Detalle objDe) {

        return objDat.ingresarDetalle(objDe.getNum_factura(), objDe.getId_producto(), objDe.getCantidad(), objDe.getPrecio(), objDe.getIva());

    }

    public int ingresarDetalleCompras(int a, int b, int c, double d, double e) {

        return objDat.ingresarDetalleCompras(a, b, c, d, e);

    }

    public int ingresarDetalleCompras2(DetalleCompras objDe) {

        return objDat.ingresarDetalleCompras(objDe.getNum_facturac(), objDe.getId_producto(), objDe.getCantidad_C(), objDe.getPrecio_c(), objDe.getIva_c());

    }

    public int ingresarProductos(Productos objPro) {

        return objDat.ingresarProductos(objPro.getId_categoria(), objPro.getNombre());

    }

    public int ingresarProveedor(Proveedor objProve) {

        return objDat.ingresarProveedor(objProve.getNombre_prove(), objProve.getDireccion());

    }

    public int ingresarCategoria(Categoria objCate) {

        return objDat.ingresarCategoria(objCate.getNombre_categ(), objCate.getDescripcion());

    }

    //MODIFICADO HASTA AQUI
    public int ingresarEmpleado(Empleado objEmple) {

        return objDat.ingresarEmpleado(objEmple.getCod_emple(), objEmple.getNombre(), objEmple.getSueldo(), objEmple.getCargo());

    }

    public int ingresarRFactura(Factura objFa) {

        return objDat.ingresarRFactura(objFa.getId_cliente(), objFa.getId_empleado(), objFa.getFecha());

    }

    public int ingresarFacturaCompras(FacturaCompras objFa) {

        return objDat.ingresarFacturaCompras(objFa.getId_proveedor(), objFa.getFecha_Com());

    }

    //CONSULTAR
    public ResultSet consultarCliente() {
        ResultSet rs = objDat.consultar_Clientes();
        return rs;
    }

    public ResultSet consultarProveedor() {
        ResultSet rs = objDat.consultarProveedor();
        return rs;
    }

    public ResultSet consultarCategoria() {
        ResultSet rs = objDat.consultarCategoria();
        return rs;
    }

    public ResultSet consultarFactura() {
        ResultSet rs = objDat.consultarFactura();
        return rs;
    }

    public ResultSet consultarFacturaCompra() {
        ResultSet rs = objDat.consultarFacturaCompra();
        return rs;
    }

    public ResultSet consultarProducto() {
        ResultSet rs = objDat.consultar_Productos();
        return rs;
    }
    
    
    

    public ResultSet consultarFacturaCliente(String cedula) {
        ResultSet rs = objDat.consultar_Factura_Cliente(cedula);
        return rs;
    }

    public ResultSet consultarFacturaCompraProveedor(String provee) {
        ResultSet rs = objDat.consultar_FacturaCompra_Provee(provee);
        return rs;
    }
    //Para Consultar y eliminar

    public ResultSet consultarDetalleEliminar(String cliente) {
        ResultSet rs = objDat.consultar_Detalle_Elim(cliente);
        return rs;
    }

    public ResultSet consultarFacturaEliminar(String cliente) {
        ResultSet rs = objDat.consultar_Factura_Elim(cliente);
        return rs;
    }
    

      public ResultSet consultarDetalleComprasEliminar(String proveedor) {
        ResultSet rs = objDat.consultar_DetalleCompra_Elim(proveedor);
        return rs;
    }

    public ResultSet consultarFacturaComprasEliminar(String proveedor) {
        ResultSet rs = objDat.consultar_FacturaCompra_Elim(proveedor);
        return rs;
    }

    public ResultSet consultarEmpleado_Clientes(int id) {
        ResultSet rs = objDat.consultar_Empleado_Clientes(id);
        return rs;
    }

    public ResultSet consultarEmpleado() {
        ResultSet rs = objDat.consultar_Empleado();
        return rs;
    }

    //MODIFICAR
    public int modificarClientes(String nombre,String telefono ,String direccion, int id) {

        int retorno = objDat.modificarClientes(nombre,telefono,direccion, id);
        return retorno;
    }

    public int modificarProductos(String nombre, int id) {

        int retorno = objDat.modificarProductos(nombre, id);
        return retorno;
    }

    public int modificarDEmpleado(String cod_emple ,String nombre,double sueldo,String cargo, int id) {

        int retorno = objDat.ModificarEmpleado(cod_emple,nombre,sueldo,cargo, id);
        return retorno;
    }
    
     public int modificarCategoria( String nombre_cate, String descripcion, int id) {

        int retorno = objDat.ModificarCategoria(nombre_cate, descripcion, id);
        return retorno;
    }
     
       public int modificarProveedor(String nombre, String direccion, int id) {

        int retorno = objDat.modificarProveedor(nombre, direccion, id);
        return retorno;
    }
    
   

    //ELIMINAR
    public int eliminarCliente(int id_cliente) {
        int retorno = objDat.eliminarCliente(id_cliente);
        return retorno;
    }

    public int eliminarProducto(int id_producto) {
        int retorno = objDat.eliminarProductos(id_producto);
        return retorno;
    }

    public int eliminarDepartamento(int id_depa) {
        int retorno = objDat.eliminarDepartamento(id_depa);
        return retorno;
    }

    public int eliminarEmpleado(int id_depa) {
        int retorno = objDat.eliminarEmpleado(id_depa);
        return retorno;
    }
    
    //Eliminar 
     public int eliminarDetalle(int id_depa) {
        int retorno = objDat.eliminarDetalle(id_depa);
        return retorno;
    }
     
      public int eliminarFactura(int id_depa) {
        int retorno = objDat.eliminarFactura(id_depa);
        return retorno;
    }
      
        public int eliminarDetalleCompras(int id_com) {
        int retorno = objDat.eliminarDetalleCompras(id_com);
        return retorno;
    }
     
      public int eliminarFacturaCompras(int id_co) {
        int retorno = objDat.eliminarFacturaCompras(id_co);
        return retorno;
    }

}
