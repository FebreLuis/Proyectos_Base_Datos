package CLASES;

/**
 *
 * @author febre
 */
public class Detalle {

    private int numero_detalle;
    private int num_factura;
    private int id_producto;
    private int cantidad;
    private double precio;
    private double iva;

    //Constructor

    public Detalle(int numero_detalle, int num_factura, int id_producto, int cantidad, double precio, double iva) {
        this.numero_detalle = numero_detalle;
        this.num_factura = num_factura;
        this.id_producto = id_producto;
        this.cantidad = cantidad;
        this.precio = precio;
        this.iva = iva;
    }
    

    public Detalle() {
    }

    //Metodos
    public int getNumero_detalle() {
        return numero_detalle;
    }

    public void setNumero_detalle(int numero_detalle) {
        this.numero_detalle = numero_detalle;
    }

    public int getNum_factura() {
        return num_factura;
    }

    public void setNum_factura(int num_factura) {
        this.num_factura = num_factura;
    }

    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getIva() {
        return iva;
    }

    public void setIva(double iva) {
        this.iva = iva;
    }

   
    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

}
