package CLASES;

/**
 *
 * @author febre
 */
public class Factura {

    private int num_factura;
    private int id_cliente;
    private int id_empleado;
    private String fecha;

    //Constructor

    public Factura(int num_factura, int id_cliente, int id_empleado, String fecha) {
        this.num_factura = num_factura;
        this.id_cliente = id_cliente;
        this.id_empleado = id_empleado;
        this.fecha = fecha;
    }
   

    //Metodos

    public int getNum_factura() {
        return num_factura;
    }

    public void setNum_factura(int num_factura) {
        this.num_factura = num_factura;
    }

    public int getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    public int getId_empleado() {
        return id_empleado;
    }

    public void setId_empleado(int id_empleado) {
        this.id_empleado = id_empleado;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
   

}
