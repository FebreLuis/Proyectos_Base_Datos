package CLASES;

/**
 *
 * @author Febre
 */
public class DetalleCompras {

    private int num_detale_com;
    private int num_facturac;
    private int id_producto;
    private int cantidad_C;
    private double precio_c;
    private double iva_c;

    //Constructor
    public DetalleCompras(int num_detale_com, int num_facturac, int id_producto, int cantidad_C, double precio_c, double iva_c) {
        this.num_detale_com = num_detale_com;
        this.num_facturac = num_facturac;
        this.id_producto = id_producto;
        this.cantidad_C = cantidad_C;
        this.precio_c = precio_c;
        this.iva_c = iva_c;
    }

    //Metodos

    public int getNum_detale_com() {
        return num_detale_com;
    }

    public void setNum_detale_com(int num_detale_com) {
        this.num_detale_com = num_detale_com;
    }

    public int getNum_facturac() {
        return num_facturac;
    }

    public void setNum_facturac(int num_facturac) {
        this.num_facturac = num_facturac;
    }

    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public int getCantidad_C() {
        return cantidad_C;
    }

    public void setCantidad_C(int cantidad_C) {
        this.cantidad_C = cantidad_C;
    }

    public double getPrecio_c() {
        return precio_c;
    }

    public void setPrecio_c(double precio_c) {
        this.precio_c = precio_c;
    }

    public double getIva_c() {
        return iva_c;
    }

    public void setIva_c(double iva_c) {
        this.iva_c = iva_c;
    }
    

}
