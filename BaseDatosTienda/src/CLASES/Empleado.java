package CLASES;

/**
 *
 * @author febre
 */
public class Empleado {

    private int id_emple;
    private String cod_emple;
    private String nombre;
    private double sueldo;
    private String cargo;

    //Constructor
    public Empleado(int id_emple, String cod_emple, String nombre, double sueldo, String cargo) {
        this.id_emple = id_emple;
        this.cod_emple = cod_emple;
        this.nombre = nombre;
        this.sueldo = sueldo;
        this.cargo = cargo;
    }

    //Metodos
    public int getId_emple() {
        return id_emple;
    }

    public void setId_emple(int id_emple) {
        this.id_emple = id_emple;
    }

    public String getCod_emple() {
        return cod_emple;
    }

    public void setCod_emple(String cod_emple) {
        this.cod_emple = cod_emple;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

}
